import React, { Component } from 'react';
import './App.css';
import Password from "./containers/Password/Password";

class App extends Component {
  render() {
    return (
      <div className="App">
        <Password/>
      </div>
    );
  }
}

export default App;
