import React, {Component} from 'react';
import {connect} from 'react-redux';
import './Password.css';
import Button from "../../components/Button/Button";

const PASSWORD = '1337';

class Password extends Component {
  render() {
    return (
      <div className="Password">
        <div className={"Password-current " + (this.props.access ? 'Password-access' : null)}>
          {this.props.access ? 'Access Granted' : this.props.password}
        </div>
        <Button onClick={() => this.props.addNumber(7)}>7</Button>
        <Button onClick={() => this.props.addNumber(8)}>8</Button>
        <Button onClick={() => this.props.addNumber(9)}>9</Button>
        <Button onClick={() => this.props.addNumber(4)}>4</Button>
        <Button onClick={() => this.props.addNumber(5)}>5</Button>
        <Button onClick={() => this.props.addNumber(6)}>6</Button>
        <Button onClick={() => this.props.addNumber(1)}>1</Button>
        <Button onClick={() => this.props.addNumber(2)}>2</Button>
        <Button onClick={() => this.props.addNumber(3)}>3</Button>
        <Button onClick={this.props.removeNumber}>&lt;</Button>
        <Button onClick={() => this.props.addNumber(0)}>0</Button>
        <Button onClick={() => this.props.passwordValidation(this.props.password)}>E</Button>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    password: state.password,
    access: state.access
  };
};

const mapDispatchToProps = dispatch => {
  return {
    addNumber: number => dispatch({type: 'ADD', value: number}),
    removeNumber: () => dispatch({type: 'REMOVE'}),
    passwordValidation: password => dispatch({
      type: 'VALIDATE',
      access: password === PASSWORD
    })
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Password);