const initialState = {
  password: '',
  access: false
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case 'ADD':
      let password = state.password;
      if (password.length < 4) {
        password = password + action.value;
      }

      return {
        ...state,
        password
      };
    case 'REMOVE':
      return {
        ...state,
        password: state.password.slice(0, -1)
      };
    case 'VALIDATE':
      return {
        ...state,
        access: action.access,
        password: ''
      };
    default:
      return state;
  }
};

export default reducer;